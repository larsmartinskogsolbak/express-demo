import $ from 'jquery';
import 'bootstrap/dist/css/bootstrap.min.css';
import '../styles/index.scss';

/**
 * Global Constants
 */
let posts = [];
let users = [];
let $base;
let $backbtn;
let $container;
let $userContainer;

/**
 * Classes
 */
class Post{
  constructor (element){
    this.id = element.id
    this.userId = element.userId
    this.title = element.title
    this.body = element.body
  }
  getTitle() {
    return this.title;
  }
  getBody() {
    return this.body;
  }
  getId() {
    return this.id;
  }
  getUserId(){
    return this.userId;
  }
}

class User{
  constructor (element){
    this.id = element.id
    this.username = element.username
    this.email = element.email
    this.name = element.name
    this.address = element.address
    this.phone = element.phone
    this.website = element.website
    this.company = element.company
  }
  getId(){
    return this.id;
  }
  getUsername(){
    return this.username;
  }
  getEmail(){
    return this.email;
  }
  getName(){
    return this.name;
  }
}
 
/**
 * Wait for the DOM to be ready
 * jQuery needs to cache to DOM :)
 */
$(document).ready(function () {
    $container = $("#app");
    $userContainer = $("#usr");
    $base = $("#base");
    $backbtn = $("#backbtn");
    fetchposts();
    $("#backbtn").hide();

    //back button that inverses the show/hide and resets the post div
    $backbtn.click((e) => {
      $("#app").show();
      $("#usr").remove();
      $("#backbtn").hide();
      $base.append(`<div id="usr" class="card"></div>`);
      $userContainer = $("#usr");
  });
})

//Fetches posts and adds them to the posts array
function fetchposts() {
  fetch('https://jsonplaceholder.typicode.com/posts')
  .then(response => response.json())
  .then(json => { json.forEach(element => posts.push(addPost(element))) 
  })
}

//adds posts to the webpage
function addPost(element){
  let post = new Post(element);
  $container.append(createCard(post));
  return post; 
  
}

//fetches a specific user from the users on webpage
function fetchUsers(userid){
  fetch('https://jsonplaceholder.typicode.com/users')
  .then(response => response.json())
  .then(json => { json.forEach(userElement => users.push(addUser(userElement, userid))) 
  })
}

//Adds usercard to the webpage
function addUser(element, userid){
  let current = new User(element);
  if (userid === current.getId()) {
    $userContainer.append(createUserCard(current));
  }
}

//Creates the usercards
function createUserCard(user) {
  return `<div class="card-body"><li> Username: ` + user.getUsername() + `</li>`+`<li> Name: ` + user.getName() + `</li>`+`<li> Email: ` + user.getEmail() + `</li></div>`
}

/**
 * Build a Card template with a picture.
 * 
 * @param {string} title
 * @param {string} body
 * @returns String
 */

 //Makes the post tables with onclick functionality.
function createCard(post) {
  return $(`<li class="list-group-item list-group-item-action">` + post.getTitle() + `</li>`).click(e => {
    $("#app").hide();
    $("#usr").show();
    $("#backbtn").show();
    fetchUsers(post.getUserId());
    $userContainer.append(`<div class="card-header"><h4 class="card-title">`+ post.getTitle() +`</h4></div><br>`+`<div class="card-body">`+post.getBody()+`</div><br>`);
  })
}
