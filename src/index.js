/* global process */
'use strict'

/**
 * Dependencies
 * @ignore
 */

 const cwd = process.cwd();
 const path = require('path')
 const morgan = require('morgan')
 const express = require('express')

 /** 
  * Doteenv
  * @ignore
   */

   require ('dotenv').config()

   /** 
  * Express
  * @ignore
   */
const { PORT: port = 3000 } = process.env
const app = express()

app.use(morgan('tiny'))
app.use(express.static(path.join(cwd, 'public')));

// GET /message
app.get('/', (req,res) =>{
res.json({
   message: 'Hello, Me!'
})
})



/**
 * Launch app
 * @ignore
 */
app.listen(port, () => console.log(`Listening on port ${port}`))